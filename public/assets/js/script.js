(function($) {
    
    "use strict";

    /* ==================== Preloader ==================== */
        function preloader_load() {
            var preLoader = $('.preloader');
            if(preLoader.length){
                preLoader.delay(400).fadeOut(500);
            }
        }

      // Scroll To Top
      $(window).on('scroll', function () {
          if ($(this).scrollTop() > 200) {
              $('.scrollup').fadeIn();
          } else {
              $('.scrollup').fadeOut();
          }
      });
      $('.scrollup').on('click', function () {
          $("html, body").animate({
              scrollTop: 0
          }, 1000);
          return false;
      });

      // Navbar
      var nav = $('.main-navigation');
      
      $(window).scroll(function () {
          if ($(this).scrollTop() > 100) {
              nav.addClass("fixed-header");
          } else {
              nav.removeClass("fixed-header");
          }
      });


    
    /*==========================================================================
        WHEN DOCUMENT LOADING
    ==========================================================================*/
    $(window).on('load', function() {

        preloader_load();

     });

    /*==========================================================================
        WHEN WINDOW READY
    ==========================================================================*/
    $(document).on('ready', function() {

    });


    /*==========================================================================
        WHEN WINDOW SCROLL
    ==========================================================================*/
    $(window).on("scroll", function() {
    });

    /*==========================================================================
        WHEN WINDOW RESIZE
    ==========================================================================*/
    $(window).on("resize", function() {

    });


})(window.jQuery);

    
       
