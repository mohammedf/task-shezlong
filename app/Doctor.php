<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    public function scopePublished() {
        return $this->where('active', true);
    }
    public function categories() {
        return $this->belongsToMany(Category::class, 'category_doctor');
    }

}
