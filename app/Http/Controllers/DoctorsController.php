<?php

namespace App\Http\Controllers;

use App\Category;
use App\Doctor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DoctorsController extends Controller
{

    public function index(Request $request)
    {
        $doctors = Doctor::with('categories')->published()->orderBy('price_30_min', 'desc')->paginate(12);
        $categories = Category::pluck('name', 'id')->toArray();
     return view('frontend.doctors.index', compact('doctors', 'categories'));

    }

    public function ajaxFilter(Request $request)
    {
        $doctors = Doctor::with('categories')->published();
        if($name = $request->doctor){
            $doctors->where('name', 'like', '%' . $name . '%');
        }
        if($request->category && $request->category != 'all'){
            $doctors->whereHas('categories', function ($q) use ($request) {
                $q->where('id', (int) $request->category);
            });
        }

        if($sort = $request->sort_price){
            $doctors->orderBy('price_30_min', $sort);
        }
        $doctors = $doctors->get();

        $view = view('frontend.doctors.partials.results', compact('doctors'))->render();
        return response()->json(['success' => true, 'message' => 'success', 'data' => $view]);
    }

    public function ajaxDoctorCard(Doctor $doctor)
    {
        return view('frontend.doctors.partials.doctor_card', compact('doctor'));
    }

}
