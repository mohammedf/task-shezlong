<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => '', 'as' => 'doctors.'], function (){
    Route::get('/', 'DoctorsController@index')->name('list');
    Route::get('/ajax/filter', 'DoctorsController@ajaxFilter')->name('filter');
    Route::get('/ajax/{doctor}/card', 'DoctorsController@ajaxDoctorCard')->name('card');


});

