<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'id' => 1,
                 'name' => 'مشاكل الأطفال',
            ],
            [
                'id' => 2,
                'name' => 'مشاكل المراهقين',
            ],
            [
                'id' => 3,
                'name' => 'الأكتئاب',
            ],
            [
                'id' => 4,
                'name' => 'القلق والوسواس',
            ],
            [
                'id' => 5,
                'name' => 'اضطربات الفصام',
            ],
            [
                'id' => 6,
                'name' => 'الإدمان',
            ],
            [
                'id' => 7,
                'name' => 'المشاكل الجنسية',
            ],
            [
                'id' => 8,
                'name' => 'مشاكل الشيخوخة',
            ]
        ]);
    }
}
