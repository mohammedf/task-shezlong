<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DoctorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobTitles = ['أخصائي توجيه وارشاد نفسي','اخصائي الطب نفسي و علاج الإدمان','استشاري الصحة النفسية','استشاري الصحة النفسية'];
        DB::table('doctors')->insert([
            [
                'id' => 1,
                'name' => 'حسين حج أحمد',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 280,
                'price_60_min' => 420,
                'active' => true,
            ],
            [
                'id' => 2,
                'name' => 'فاطمة قطب',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 260,
                'price_60_min' => 430,
                'active' => true,
            ],
            [
                'id' => 3,
                'name' => 'علاء العشري',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 180,
                'price_60_min' => 300,
                'active' => true,
            ],
            [
                'id' => 4,
                'name' => 'محمد سراج',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 200,
                'price_60_min' => 290,
                'active' => true,
            ],
            [
                'id' => 5,
                'name' => 'سمر حمدي',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 210,
                'price_60_min' => 410,
                'active' => true,
            ],
            [
                'id' => 6,
                'name' => 'وئام العايدي',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 220,
                'price_60_min' => 400,
                'active' => true,
            ],
            [
                'id' => 7,
                'name' => 'روز عبدالله',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 150,
                'price_60_min' => 300,
                'active' => true,
            ],
            [
                'id' => 8,
                'name' => 'يارا فيصل',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 130,
                'price_60_min' => 260,
                'active' => true,
            ],
            [
                'id' => 9,
                'name' => 'شيماء ثابت',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 170,
                'price_60_min' => 250,
                'active' => true,
            ],
            [
                'id' => 10,
                'name' => 'زكريا فاخوري',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 190,
                'price_60_min' => 310,
                'active' => true,
            ],
            [
                'id' => 11,
                'name' => 'شيماء جابر عبد الراضي',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 170,
                'price_60_min' => 330,
                'active' => true,
            ],
            [
                'id' => 12,
                'name' => 'عبدالرحمن عبدالله العمري',
                'description' => $jobTitles[array_rand($jobTitles, 1)],
                'price_30_min' => 250,
                'price_60_min' => 420,
                'active' => true,
            ],

        ]);

        $doctors = DB::table('doctors')->get();
        $categoryDoctor = [];
        foreach ($doctors as $doctor){
            $categoryDoctor[] = [
                'category_id' => rand(1, 4),
                'doctor_id' => $doctor->id,
            ];
            $categoryDoctor[] = [
                'category_id' => rand(5, 8),
                'doctor_id' => $doctor->id,
            ];
         }
            DB::table('category_doctor')->insert($categoryDoctor);


    }
}
