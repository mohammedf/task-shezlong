<!-- Header Section Start -->
<header class="header header-style-1 clearfix">

    <div class="menu-style bg-transparent menu-hover-2  clearfix">
        <!-- main-navigation start -->
        <div class="main-navigation main-mega-menu animated">
            <nav class="navbar navbar-expand-lg navbar-light">

                <div class="container">
                    <!-- header dropdown buttons end-->
                    <a class="navbar-brand" href="#">
                        <img id="logo_img" src="{{asset('assets/shezlong-logo-ar.png')}}" alt="">
                    </a>

                    <button class="navbar-toggler" type="button" data-toggle="collapse"data-target="#navbar-collapse-1" aria-controls="navbar-collapse-1" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>


                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <!-- main-menu -->
                        <ul class="navbar-nav ml-xl-auto">

                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">المعالجين </a>
                            </li>


                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">ازاي تحجز جلستك؟ </a>
                            </li>

                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">الاختبارات النفسية</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a href="#" class="nav-link"data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">المقالات </a>
                            </li>
                        </ul>


                        <a class="nav-link">تسجيل الدخول</a>
                        <a class="nav-link"> انضم الينا كطبيب</a>


                        <a class="nav-link">English <i class="fa fa-globe" aria-hidden="true"></i></a>

                    </div>
                </div>
            </nav>
        </div>
        <!-- main-navigation end -->
    </div>

</header>
<!-- Header Section End -->
