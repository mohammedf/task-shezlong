<section class="footer-copy-right bg-theme-color-2 text-white p-0">
    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <p class="text-white">© 2019, All Rights Reserved, Design & Developed By:<a href="#"> MFathi </a></p>
            </div>
        </div>
    </div>
</section>
<!-- Footer Style Seven End -->
