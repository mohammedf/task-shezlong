<section class="steps">
    <div  class="container align-items-center">
        <div  class="row animated slideInDown justify-content-center">
            <div class="col-md-12 text-center text-white fz24 mb-30">3 خطوات نحو حياة افضل</div>
            <div  class="col-md-2 step active">اختار المعالج</div>
            <div class="col-md-1 seprator"></div>
            <div class="col-md-2 step">حدد التاريخ والوقت</div>
            <div class="col-md-1 seprator"></div>
            <div class="col-md-2 step">أكمل عملية الدفع</div>
        </div>
    </div>
</section>
