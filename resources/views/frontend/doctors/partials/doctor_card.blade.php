@php
    $doctorCategoriesArray = $doctor->categories->pluck('name')->toArray();
@endphp
<div class="row">

    <div class="col-md-12">

        <div class="row">
            <div class="col-md-4">
                <div class="thumb">
                    <a href="#"> <img
                            src="{{asset('media/'.$doctor->id.'.png')}}"
                            alt=""></a>
                </div>
            </div>
            <div class="col-md-8">
                <div class="content">
                    <div class="title-box">
                        <h4 class="doctor-grid-title text-blue">{{$doctor->name}}</h4>
                        <h3 class="doctor-desc">{{$doctor->description}}</h3>
                    </div>
                    <div class="review-stars row justify-content-center">
                        <div class="col-md-12 text-center-col mt-10">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <span>
4.82
(
220 تقييم)</span>

                        </div>

                        <div class="col-md-12 text-center-col">{{implode(" ,", $doctorCategoriesArray)}}</div>


                    </div>

                    <div class="mt-15">
                        <div class="text-center">
                            <i class="fa fa-money" style="color: #3bb54a;"></i>

                            <span class="pr-10"> جنيه {{$doctor->price_30_min}}/ 30 دقيقة   </span>-<span
                                class="pl-10">   جنيه  جنيه {{$doctor->price_60_min}}/ 60 دقيقة</span>
                        </div>

                    </div>


                </div>

            </div>

        </div>

        <div class="row">
            <div class="col-md-12 p-0 mt-20">
                <div class="buttons-group">
                    <div class="book pt-10 pb-10">This Week</div>
                    <div class="calender pt-10 pb-10 " tabindex="0">Calender</div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12 mt-10">
                <div class="d-flex justify-content-between">
                    <span class="font-bold font-size-14 text-black">برجاء اختيار موعد</span>
                    <span class="text-right text-black"><i class="fa fa-globe"></i> جميع المواعيد (Africa/Cairo) </span>
                </div>
            </div>
            <div class="col-md-4">

            </div>
        </div>


    </div>


</div>
