@extends('frontend.layouts.master')

@section('content')
    @include('frontend.doctors.partials.steps_section')
<!-- Doctors Section Start -->
<section class="blog-section bg-f8 pt-30">
    <div class="container">
<form id="get-doctors-results-form" action="{{route('doctors.filter')}}">
        <div id="cbp-vm" class="cbp-vm-switcher cbp-vm-view-grid">

            <div class="row">

                <div class="col-md-3">

                    <div class="checkbox-container circular-container">

                        <h3 class="title-filter-box">التصنيف <span class="pull-left">مسح التصنيف</span></h3>

                        <div class="sidebar-section-title">

                            <svg-icon src="assets/images/search/time.svg" class="mr8 filter-icon"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 50 50" style="width: 25px;"><defs><style>.cls-1-clock{fill:#3da8c0;}.cls-2-clock,.cls-3-clock{fill:none;stroke:#fff;stroke-miterlimit:10;stroke-width:2px;}.cls-3-clock{stroke-linecap:round;}</style></defs><title>time</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><circle class="cls-1-clock" cx="25" cy="25" r="25"></circle><polyline class="cls-2-clock" points="25 11.21 25 25.44 37.85 31.29"></polyline><line class="cls-3-clock" x1="25" y1="4.24" x2="25" y2="5.61"></line><line class="cls-3-clock" x1="25" y1="45.15" x2="25" y2="46.52"></line><line class="cls-3-clock" x1="46.14" y1="25.38" x2="44.77" y2="25.38"></line><line class="cls-3-clock" x1="5.23" y1="25.38" x2="3.86" y2="25.38"></line><line class="cls-3-clock" x1="39.95" y1="10.43" x2="38.98" y2="11.4"></line><line class="cls-3-clock" x1="11.02" y1="39.36" x2="10.05" y2="40.32"></line><line class="cls-3-clock" x1="39.95" y1="40.32" x2="38.98" y2="39.36"></line><line class="cls-3-clock" x1="11.02" y1="11.4" x2="10.05" y2="10.43"></line></g></g></svg></svg-icon>
                            <span>المواعيد المتاحة والمدة</span>
                        </div>
                        <div class="d-flex">
                            <label class="checkbox-label">
                                <input type="checkbox">
                                <span class="checkbox-custom circular"></span>
                                <strong>اليوم</strong>
                            </label>
                            <label class="checkbox-label">
                                <input type="checkbox">
                                <span class="checkbox-custom circular"></span>
                                <strong>الاسبوع</strong>
                            </label>
                        </div>
                        <div class="mt-20">
                            <p>متاح من : اختر ميعاد</p>
                            <input type="text" name="date" class="form-control" placeholder="اختر التاريخ">
                        </div>

                        <div class="mt-20">
                            <p>متاح من : اختر ميعاد</p>
                            <input type="text" name="date" class="form-control" disabled placeholder="اختر التاريخ">
                        </div>

                        <p class="mt-20">المدة</p>

                        <div class="d-flex">
                            <label class="checkbox-label">
                                <input type="checkbox">
                                <span class="checkbox-custom circular"></span>
                                <strong>30 دقيقة</strong>
                            </label>
                            <label class="checkbox-label">
                                <input type="checkbox">
                                <span class="checkbox-custom circular"></span>
                                <strong>60 دقيقة</strong>
                            </label>
                        </div>

                    </div>
                    <!-- <div class="clear"></div> -->
                </div>
                <div class="col-md-9 cbp-vm-view-grid">
                    <div class="tools-ber">
                        <div class="row">
                            <div class="col-lg-4">
                                <!-- filters select -->
                                <div class="select-filters">

                                    <input type="text" name="doctor" class="form-control filter-input-name"  placeholder="&#xF002; بحث بأسم المعالج" style="font-family:Arial, FontAwesome">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!-- filters select -->
                                <div class="select-filters">
                                    <select class="sort_price filter-input" id="sort-category" name="category">
                                        <option value="all">جميع التخصصات</option>
                                        @foreach($categories as $key => $value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <!-- filters select -->
                                <div class="select-filters">
                                    <select name="sort_price" id="sort-price" class="filter-input">
                                        <option disabled="">رتب حسب</option>
                                        <option value="asc">السعر: من الأقل الى الأعلى</option>
                                        <option value="desc">السعر: من الأعلى الى الأقل</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row" id="doctors-section">
                       @foreach($doctors as $doctor)
                        <div class="col-lg-4">
                            @include('frontend.doctors.partials.doctor_grid')
                        </div>
                       @endforeach
                    </div>
                </div>

            </div>
        </div>


</form>
    </div>

</section>
<!-- Doctors Section End -->
    @endsection

    @section('js')
    <script>

        $(".filter-input").on('change',function(e) {

            e.preventDefault();

            getResults();


        });

        $(".filter-input-name").on('keyup',delayKeyUp(function(e) {
            getResults();


        }, 500));

        function getResults() {
            var form = $('#get-doctors-results-form');
            var url = form.attr('action');

            $.ajax({
                type: "GET",
                url: url,
                data: form.serialize(),
                success: function(response)
                {
                    $('#doctors-section').html(response.data)
                }
            });

        }

        function delayKeyUp(callback, ms) {
            var timer = 0;
            return function() {
                var context = this, args = arguments;
                clearTimeout(timer);
                timer = setTimeout(function () {
                    callback.apply(context, args);
                }, ms || 0);
            };
        }
        // $(".filter-input-name").on('click', function(e) {
        //     e.preventDefault();
        //      $('#doctorModal').modal()
        // });

        $('body').on("click", ".open-doctor-modal", function(e) {
            e.preventDefault();
            var btn = $(this);
            $("#doctorModal").modal().find(".modal-body").load(btn.data('url'));
        });


    </script>
    @endsection
