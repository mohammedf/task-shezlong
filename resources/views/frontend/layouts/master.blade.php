<!DOCTYPE html>
<html lang="en">


<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Shezlong Task</title>
@include('frontend.partials.scripts.header')
<!-- Favicon -->
    <link href="{{asset('favicon.ico')}}" rel="icon" type="image/x-icon">
    @yield('css')
</head>

<body dir="rtl">


<!-- Start Page Wrapper  -->
<div class="page-wrapper">
@include('frontend.partials.header')
@yield('content')

<!-- Modal -->
    <div class="modal left fade" id="doctorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-body">

                </div>

            </div><!-- modal-content -->
        </div><!-- modal-dialog -->
    </div><!-- modal -->


    @include('frontend.partials.footer')

</div>
<!-- End Page Wrapper  -->


<a href="#" class="scrollup"><i class="flaticon-long-arrow-pointing-up" aria-hidden="true"></i></a>

@include('frontend.partials.scripts.footer')
@yield('js')

</body>


</html>
